﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Runtime;

namespace cramo_cra3000_items.Models
{
    
    public class GetPrices
    {
        public string MarketId { get; set; }
        public string TransactionId { get; set; }

        public List<Item> Items { get; set; }
    }
}