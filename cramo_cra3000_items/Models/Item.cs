﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cramo_cra3000_items.Models
{
    public class Item
    {
        public string MarketId { get; set; }
        public string ItemNumber { get; set; }
        public double Weight { get; set; }
        public string Brandtype { get; set; }

    }
}