﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace cramo_cra3000_items.Models
{
    [Serializable]
    [DataContract (Name="GetPrices", Namespace=@"cramo:http://www.cramo.com/canonical")]
    public class GetPricesRental
    {
        public DateTime CreationDateTime { get; set; }

        public GetPricesRental()
        {
            CreationDateTime = DateTime.Now;
        }
    }
}