﻿using Cramo.Utils.Transformations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace cramo_cra3000_items.Models
{
    public static class PriceManager
    {


        private static Transformer rentalPriceRequestTransformer;
        private static Transformer salesPriceRequestTransformer;
        private static string baseAddress;
        private static string getPriceServiceUri;
        private static string getTimeUnitPriceServiceUri;
        private static string subscriptionKey;
        private static Transformer showSalesPriceTransformer;
        private static Transformer showRentalPriceTransformer;


        public static void Initialize()
        {

            rentalPriceRequestTransformer = new Transformer("cramo_cra3000_items.resources.GetPrices_To_GetTimeUnitPrices_or_GetPrices.xslt", Encoding.UTF8, true, true);
            salesPriceRequestTransformer = new Transformer("cramo_cra3000_items.resources.GetPrices_To_GetSalesPrices.xslt", Encoding.UTF8, true, true);
            showRentalPriceTransformer = new Transformer("cramo_cra3000_items.resources.360Price_To_ShowPrices_v4.xslt", Encoding.UTF8, true, true);
            showSalesPriceTransformer = new Transformer("cramo_cra3000_items.resources.360Price_To_ShowPrices_salesprice.xslt", Encoding.UTF8, true, true);

            baseAddress = System.Configuration.ConfigurationManager.AppSettings["360.BaseAddress"];
            getTimeUnitPriceServiceUri = System.Configuration.ConfigurationManager.AppSettings["360.Uri.GetTimeUnitPrices"];
            getPriceServiceUri = System.Configuration.ConfigurationManager.AppSettings["360.Uri.GetPrices"];

            subscriptionKey = System.Configuration.ConfigurationManager.AppSettings["360.SubscriptionKey"];

        }

        public static Transformer RentalPriceRequestTransformer
        {
            get
            {
                return rentalPriceRequestTransformer;
            }
        }

        public static Transformer SalesPriceRequestTransformer
        {
            get
            {
                return salesPriceRequestTransformer;
            }
        }

        public static string SubscriptionKey
        {
            get
            {
                return subscriptionKey;
            }

        }

        public static string BaseAddress
        {
            get
            {
                return baseAddress;
            }

        }

        public static string GetPriceServiceUri
        {
            get
            {
                return getPriceServiceUri;
            }

        }

        public static string GetTimeUnitPriceServiceUri
        {
            get
            {
                return getTimeUnitPriceServiceUri;
            }
        }

        public static Transformer ShowSalesPricesTransformer
        {
            get
            {
                return showSalesPriceTransformer;
            }

        }

        public static Transformer ShowRentalPricesTransformer
        {
            get
            {
                return showRentalPriceTransformer;
            }

        }
    }
}