﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace cramo_cra3000_items.Models
{
    public class Price
    {

        private PriceType _priceType;
        private string _service;
        private string _oagisPriceRequest;
        private string _oagisPriceResponse;
        private bool? status;

        public string Response
        {
            get
            {
                return _oagisPriceResponse;
            }

        }

        public bool? Status
        {
            get
            {
                return status;
            }

        }

        public Price(PriceType priceType, string priceRequest)
        {

            this._priceType = priceType;

            string priceEnquiry = PrepareRequestMessageFromPostBody(priceRequest);

            if (priceType == PriceType.RentalPrice)
            {
                _oagisPriceRequest = PriceManager.RentalPriceRequestTransformer.Transform(priceEnquiry);

                XDocument oagis = XDocument.Load(new StringReader(_oagisPriceRequest));

                this._service = oagis.Root.Name.LocalName;

            }
            else
            {
                _oagisPriceRequest = PriceManager.SalesPriceRequestTransformer.Transform(priceEnquiry);
                this._service = "GetPrices";
            }

        }

        public Price(PriceType priceType, string marketId, string productId, string customerId, string depotId, string fromDate, string toDate, string timeUnit, string quantity)
        {
            this._priceType = priceType;

            string priceEnquiry = PrepareRequestMessageFromGetQuery(marketId, productId, customerId, depotId, fromDate, toDate, timeUnit, quantity);

            if (priceType == PriceType.RentalPrice)
            {
                _oagisPriceRequest = PriceManager.RentalPriceRequestTransformer.Transform(priceEnquiry);

                XDocument oagis = XDocument.Load(new StringReader(_oagisPriceRequest));

                this._service = oagis.Root.Name.LocalName;

            }
            else
            {
                _oagisPriceRequest = PriceManager.SalesPriceRequestTransformer.Transform(priceEnquiry);
                this._service = "GetPrices";
            }

        }

        public async Task<bool?> GetPrice()
        {
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri(PriceManager.BaseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/xml"));

                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", PriceManager.SubscriptionKey);

                string uri;

                if (this._service == "GetPrices")
                    uri = PriceManager.GetPriceServiceUri;
                else
                    uri = PriceManager.GetTimeUnitPriceServiceUri;

                Trace.TraceInformation($"Sending POST {uri}");
                Trace.TraceInformation(this._oagisPriceRequest);
                HttpResponseMessage response = await client.PostAsync(uri, new StringContent(_oagisPriceRequest));

                if (!response.IsSuccessStatusCode)
                {
                    status = false;
                    Trace.TraceError("Exception caught while sending price request to Rental.");
                    Trace.TraceError(response.StatusCode.ToString());
                    Trace.TraceError(response.Content.ReadAsStringAsync().Result);

                    _oagisPriceResponse = response.Content.ReadAsStringAsync().Result;

                }
                else
                {

                    Trace.TraceInformation(response.StatusCode.ToString());
                    Trace.TraceInformation(response.Content.ReadAsStringAsync().Result);

                    status = true;

                    if (this._priceType == PriceType.RentalPrice)
                        _oagisPriceResponse = PriceManager.ShowRentalPricesTransformer.Transform(response.Content.ReadAsStringAsync().Result);
                    else
                        _oagisPriceResponse = PriceManager.ShowSalesPricesTransformer.Transform(response.Content.ReadAsStringAsync().Result);

                }

                return status;

            }
        }

        private string PrepareRequestMessageFromGetQuery(string marketId, string productId, string customerId, string depotId, string fromDate, string toDate, string timeUnit, string quantity)
        {

            Trace.TraceInformation("Preparing request message from GET.");

            XDocument message = new XDocument(
                new XElement("GetPrices",
                    new XElement("MarketId", marketId),
                    new XElement("NewTransactionId", Guid.NewGuid().ToString()),
                    new XElement("CreationDateTime", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss")),
                    new XElement("Items",
                        new XElement("Item",
                            new XElement("ItemNo", productId),
                            new XElement("CustomerId", customerId),
                            new XElement("DepotId", depotId),
                            new XElement("FromDate", fromDate),
                            new XElement("ToDate", toDate),
                            new XElement("TimeUnit", timeUnit),
                            new XElement("Quantity", quantity)
                            ))));

            Trace.TraceInformation("Prepared message:");
            Trace.TraceInformation(message.ToString());

            return message.ToString();

        }



        private string PrepareRequestMessageFromPostBody(string priceBody)
        {

            Trace.WriteLine("Preparing request message from POST.");

            XmlDocument xdoc = new XmlDocument();

            xdoc.LoadXml(priceBody);

            XmlElement root = xdoc.DocumentElement;

            // if transactionId is provided - use it. Otherwise create new.
            XmlNode transId = xdoc.SelectSingleNode("/GetPrices/TransactionId");
            XmlElement _xguid = xdoc.CreateElement("NewTransactionId");

            if (transId != null && transId.InnerText.Length > 0)
            {
                _xguid.InnerText = transId.InnerText;
                //Trace.TraceInformation("TransactionId found in message: " + _transactionId);
            }
            else
            {
                _xguid.InnerText = Guid.NewGuid().ToString();
                //Trace.TraceInformation("Created new transactionId: " + _transactionId);
            }

            XmlElement _xdate = xdoc.CreateElement("CreationDateTime");
            _xdate.InnerText = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");

            root.InsertAfter(_xdate, root.LastChild);
            root.InsertAfter(_xguid, root.LastChild);

            Trace.TraceInformation("Prepared message:");
            Trace.TraceInformation(xdoc.OuterXml);

            return xdoc.OuterXml;

        }

    }
}