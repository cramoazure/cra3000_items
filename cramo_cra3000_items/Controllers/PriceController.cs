﻿using System.Xml;
using System.Diagnostics;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Cramo.Utils.Logging;
using Cramo.Utils.Authentication.Filters;
using Cramo.Utils.Transformations;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using cramo_cra3000_items.Models;
using System.Xml.Linq;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;

namespace cramo_cra3000_items.Controllers
{
    [IdentityBasicAuthentication]
    [Authorize]
    public class PriceController : ApiController
    {

        [Route("api/RentalPrice")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetRentalPrice(string marketId, string productId, string customerId, string depotId, string fromDate, string toDate = null, string timeUnit = null, string quantity = "1")
        {
            Trace.TraceInformation($"Received GET {Request.RequestUri.PathAndQuery}");

            Price p = new Price(PriceType.RentalPrice, marketId, productId, customerId, depotId, fromDate, toDate, timeUnit, quantity);

            bool? b = await p.GetPrice();

            return GetHttpResponseMessage(p, Request);

        }

        [Route("api/RentalPrice")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetRentalPrices([FromBody]string priceBody)
        {
            Trace.TraceInformation($"Received POST {Request.RequestUri.PathAndQuery}");
            Trace.TraceInformation(priceBody);

            Price p = new Price(PriceType.RentalPrice, priceBody);

            bool? b = await p.GetPrice();

            return GetHttpResponseMessage(p, Request);

        }

        [Route("api/SalesPrice")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetSalesPrice(string marketId, string productId, string customerId, string depotId, string fromDate, string toDate = null, string timeUnit = null, string quantity = "1")
        {
            Trace.TraceInformation($"Received GET {Request.RequestUri.PathAndQuery}");

            Price p = new Price(PriceType.SalesPrice, marketId, productId, customerId, depotId, fromDate, toDate, timeUnit, quantity);

            bool? b = await p.GetPrice();

            return GetHttpResponseMessage(p, Request);

        }

        [Route("api/SalesPrice")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetSalesPrices([FromBody]string priceBody)
        {
            Trace.TraceInformation($"Received POST {Request.RequestUri.PathAndQuery}");
            Trace.TraceInformation(priceBody);

            Price p = new Price(PriceType.SalesPrice, priceBody);

            bool? b = await p.GetPrice();

            return GetHttpResponseMessage(p, Request);

        }

        private HttpResponseMessage GetHttpResponseMessage(Price p, HttpRequestMessage r)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            if ((bool)p.Status)
            {
                response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.StatusCode = HttpStatusCode.InternalServerError;
            }

            HttpContent responseContent;

            if (r.Headers.Accept.ToString() == "application/json")
            {
                XDocument xdoc = XDocument.Load(new StringReader(p.Response));
                responseContent = new StringContent(JsonConvert.SerializeXNode(xdoc), Encoding.UTF8, "application/json");
            }
            else
            {
                responseContent = new StringContent(p.Response, Encoding.UTF8, "application/xml");
            }

            response.Content = responseContent;

            return response;

        }


    }
}