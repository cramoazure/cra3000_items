﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Cramo.Utils.Authentication.Filters;

namespace cramo_cra3000_items.Controllers
{
    [Authorize]
    [IdentityBasicAuthentication]
    public class ServiceObjectsController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string messageBody)
        {
            return new HttpResponseMessage(HttpStatusCode.Accepted);
        }

        [HttpGet]
        public HttpResponseMessage Get(string marketId, string productId, string id)
        {
            return new HttpResponseMessage(HttpStatusCode.OK);
        }


    }
}
