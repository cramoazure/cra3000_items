﻿using System.Xml;
using System.Diagnostics;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Cramo.Utils.Logging;
using Cramo.Utils.Authentication.Filters;
using Cramo.Utils.Transformations;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Xml.Linq;
using System.IO;

namespace cramo_cra3000_items.Controllers
{
    [IdentityBasicAuthentication]
    [Authorize]
    public class AssortmentController : ApiController
    {

        private Logger _logger;
        private string _transactionId;
        private string _marketId;


        [HttpPost]
        public async Task<HttpResponseMessage> Post([FromBody]string messageBody)
        {

            Trace.TraceInformation($"Received POST {Request.RequestUri.PathAndQuery}");
            Trace.TraceInformation(messageBody);

            _logger = new Logger(this.GetType().Name);

            _logger.Log(Loglevel.Trace, string.Empty, "0", "Assortment request received.", messageBody);

            XmlDocument xdoc = new XmlDocument();

            xdoc.LoadXml(messageBody);

            XmlElement root = xdoc.DocumentElement;

            // Check if transactionId is provided. If not - create a new one and add to request message

            XmlNode transId = xdoc.SelectSingleNode("/GetAssortments/TransactionId");
            XmlNode marketId = xdoc.SelectSingleNode("GetAssortments/MarketId");
            _marketId = marketId.InnerText;

            if (transId != null && transId.InnerText.Length > 0)
            {
                _transactionId = transId.InnerText;
                Debug.WriteLine("TransactionId found in message: " + _transactionId);
            }
            else
            {
                _transactionId = Guid.NewGuid().ToString();
                Debug.WriteLine("Created a new transactionId: " + _transactionId);
            }

            XmlElement _xguid = xdoc.CreateElement("NewTransactionId");
            _xguid.InnerText = _transactionId;

            XmlElement _xdate = xdoc.CreateElement("CreationDateTime");
            _xdate.InnerText = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");

            root.InsertAfter(_xguid, root.LastChild);
            root.InsertAfter(_xdate, root.LastChild);

            _logger.Log(Loglevel.Trace, _transactionId, "10", "Assortment request received and prepared.", xdoc.OuterXml);

            Transformer requesttransformer = new Transformer("cramo_cra3000_items.resources.GetAssortments_To_GetItemAssortments.xslt", Encoding.UTF8, true, true);

            string _360request = requesttransformer.Transform(xdoc.OuterXml);

            HttpResponseMessage rentalResponse = await PostAssortmentRequest(_360request);

            if (rentalResponse.IsSuccessStatusCode)
            {

                string _360response = rentalResponse.Content.ReadAsStringAsync().Result;

                _logger.Log(Loglevel.Trace, _transactionId, "30", "Response received.", _360response);

                Transformer responsetransformer = new Transformer("cramo_cra3000_items.resources.SyncItemMaster_To_ShowAssortments.xslt", Encoding.UTF8, true, true);

                string response_content = responsetransformer.Transform(_360response);

                Trace.TraceInformation($"Transformed response: {response_content}");

                HttpResponseMessage response = new HttpResponseMessage();

                if (Request.Headers.Accept.ToString() == "application/json")
                {
                    //ShowAssortment_v2.xsd
                    response.Content = new StringContent(JsonConvert.SerializeXNode(XDocument.Load(new StringReader(response_content))));
                }
                else
                {
                    response.Content = new StringContent(response_content);
                }

                response.StatusCode = HttpStatusCode.OK;

                return response;
            }
            else
            {
                Trace.TraceError("Returning non-successful response.");
                return rentalResponse;
            }

        }


        private async Task<HttpResponseMessage> PostAssortmentRequest(string content)
        {

            using (var client = new HttpClient())
            {

                string baseAddress = System.Configuration.ConfigurationManager.AppSettings["360.BaseAddress"];
                string uri = System.Configuration.ConfigurationManager.AppSettings["360.Uri.Assortment"];
                string subscriptionKey = System.Configuration.ConfigurationManager.AppSettings["360.SubscriptionKey"];

                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/xml"));

                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", subscriptionKey);

                Trace.TraceInformation($"Sending POST {uri} at {client.BaseAddress}");
                Trace.TraceInformation(content);

                HttpResponseMessage rentalResponse = await client.PostAsync(uri, new StringContent(content));

                if (!rentalResponse.IsSuccessStatusCode)
                {
                    if (rentalResponse.StatusCode == HttpStatusCode.NotFound)
                    {
                        Trace.TraceWarning($"Rental says {rentalResponse.StatusCode.ToString()}");
                        Trace.TraceWarning($"Rental says {rentalResponse.Content.ReadAsStringAsync().Result}");

                        _logger.Log(Loglevel.Information, _transactionId, "900", $"Rental says {rentalResponse.StatusCode.ToString()} while sending request to {uri}", rentalResponse.StatusCode.ToString() + " " + await rentalResponse.Content.ReadAsStringAsync());

                        HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                        response.Content = new StringContent(GetEmptyMessage());

                    }
                    else
                    {
                        Trace.TraceError($"Rental says {rentalResponse.StatusCode.ToString()}");
                        Trace.TraceError($"Rental says {rentalResponse.Content.ReadAsStringAsync().Result}");

                        _logger.Log(Loglevel.Error, _transactionId, "900", $"Rental says {rentalResponse.StatusCode.ToString()} while sending request to {uri}", rentalResponse.StatusCode.ToString() + " " + await rentalResponse.Content.ReadAsStringAsync());

                    }

                   
                }
                else
                {
                    Trace.TraceInformation($"Rental says {rentalResponse.StatusCode.ToString()}");
                    Trace.TraceInformation($"Rental says {rentalResponse.Content.ReadAsStringAsync().Result}");
                }

                return rentalResponse;

            }

        }

        private string GetEmptyMessage()
        {
            return string.Format("<SyncItemMaster xmlns=\"http://www.openapplications.org/oagis/10\" releaseID=\"10\" versionID=\"1\" systemEnvironmentCode=\"TEST\" languageCode=\"sv\"><ApplicationArea><Sender><ReferenceID>{0}</ReferenceID></Sender><CreationDateTime>{1}</CreationDateTime><BODID>{2}</BODID></ApplicationArea><DataArea><Sync/></DataArea></SyncItemMaster>", _marketId, DateTime.Now.ToString("yyyyMMddThh:mm:sszzz"),_transactionId);
        }
    }
}