﻿using Cramo.Utils.Authentication.Filters;
using Cramo.Utils.Logging;
using Cramo.Utils.Transformations;
using Cramo.Utils.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Xml;
using System.Xml.Linq;


namespace cramo_cra3000_items.Controllers
{

    [IdentityBasicAuthentication]
    [Authorize]
    public class AvailabilityController : ApiController
    {

        private Logger _logger;
        private string _transactionId;
        private string _marketId;

        [HttpPost]
        public async Task<HttpResponseMessage> Post([FromBody]string messageBody)
        {

            Debug.WriteLine(messageBody);

            _logger = new Logger(this.GetType().Name);

            _logger.Log(Loglevel.Trace, string.Empty, "0", "Availability request received.", messageBody);

            XmlDocument xdoc = new XmlDocument();

            xdoc.LoadXml(messageBody);

            XmlElement root = xdoc.DocumentElement;

            // if transactionId is provided - use it. Otherwise create new.

            XmlNode transId = xdoc.SelectSingleNode("/GetAvailability/TransactionId");

            XmlNode marketId = xdoc.SelectSingleNode("GetAvailability/MarketId");
            _marketId = marketId.InnerText;


            if (transId != null && transId.InnerText.Length > 0)
            {
                _transactionId = transId.InnerText;
                Debug.WriteLine("TransactionId found in message: " + _transactionId);
            }
            else
            {
                _transactionId = Guid.NewGuid().ToString();
                Debug.WriteLine("Created new transactionId: " + _transactionId);
            }

            XmlElement _xdate = xdoc.CreateElement("CreationDateTime");
            _xdate.InnerText = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");

            XmlElement _xguid = xdoc.CreateElement("NewTransactionId");
            _xguid.InnerText = _transactionId;

            root.InsertAfter(_xdate, root.LastChild);
            root.InsertAfter(_xguid, root.LastChild);

            _logger.Log(Loglevel.Trace, _transactionId, "10", "Availability request received and prepared.", xdoc.OuterXml);

            Transformer t = new Transformer("cramo_cra3000_items.resources.GetAvailability_To_GetItemAvailabilitys.xslt", Encoding.UTF8, true, true);

            string _360request = t.Transform(xdoc.OuterXml);

            Debug.WriteLine(_360request);
            _logger.Log(Loglevel.Trace, _transactionId, "20", "Availability request transformed.", _360request);


            string _360response = await Call360Async(_360request);

            Debug.WriteLine(_360response);
            _logger.Log(Loglevel.Trace, _transactionId, "30", "Response received.", _360response);

            Transformer t2 = new Transformer("cramo_cra3000_items.resources.ShowItemAvailabilitys_To_ShowAvailability_v2.xslt", Encoding.UTF8, true, true);

            string response_content = t2.Transform(_360response);

            HttpResponseMessage response = new HttpResponseMessage();

            if (Request.Headers.Accept.ToString() == "application/json")
            {

                //var xml = XElement.Parse(response_content);
                //string jsonResult = JsonConvert.SerializeXNode(xml);
                try
                {

                    response.Content = new StringContent(JsonConverter.GetJsonString(response_content, "cramo_cra3000_items.Schemas.ShowAvailability.xsd"));

                }
                catch (Exception e)
                {
                    throw;
                }
            }
            else
            {
                response.Content = new StringContent(response_content);
            }

            response.StatusCode = HttpStatusCode.OK;

            return response;

        }

        private async Task<string> Call360Async(string content)
        {

            using (var client = new HttpClient())
            {

                string baseAddress = System.Configuration.ConfigurationManager.AppSettings["360.BaseAddress"];
                string uri = System.Configuration.ConfigurationManager.AppSettings["360.Uri.Availability"];
                string subscriptionKey = System.Configuration.ConfigurationManager.AppSettings["360.SubscriptionKey"];

                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/xml"));

                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", subscriptionKey);

                HttpResponseMessage response = await client.PostAsync(uri, new StringContent(content));

                if (!response.IsSuccessStatusCode)
                {
                    _logger.Log(Loglevel.Error, _transactionId, "900", "Exception caught while sending request to 360", response.StatusCode.ToString() + " " + await response.Content.ReadAsStringAsync());

                    return GetEmptyMessage();
                }

                return await response.Content.ReadAsStringAsync();

            }

        }

        private string GetEmptyMessage()
        {
            return string.Format("<ShowItemAvailabilitys xmlns=\"http://www.cramo.com/canonical\" releaseID=\"10\" versionID=\"1\" systemEnvironmentCode=\"TEST\" languageCode=\"en\"><ApplicationArea><Sender><ReferenceID>{0}</ReferenceID></Sender><CreationDateTime>{1}</CreationDateTime><BODID>{2}</BODID></ApplicationArea><DataArea/></ShowItemAvailabilitys>", _marketId, DateTime.Now.ToString("yyyyMMddThh:mm:sszzz"), _transactionId);
        }




    }
}
