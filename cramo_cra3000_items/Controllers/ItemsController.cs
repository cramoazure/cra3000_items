﻿using Cramo.Utils.Logging;
using Cramo.Utils.Transformations;
using cramo_cra3000_items.Models;
using inRiver.Remoting.Objects;
using iStone.inRiver.Core.Client.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using System.Xml;
using System.Xml.Linq;

namespace cramo_cra3000_items.Controllers
{
    public class ItemsController : ApiController
    {
        private Logger _logger;
        private string _logkey;


        [HttpPost]
        public async Task<HttpResponseMessage> Post([FromBody]string messageBody)
        {
            _logger = new Logger(this.GetType().Name);
            _logger.Log(Loglevel.Trace, string.Empty, "0", "Item http POST request received.", messageBody);

            return await SendRequestToBackend(PrepareAndTransformMessage(messageBody, "New Item"));

        }

        [HttpPut]
        public async Task<HttpResponseMessage> Put([FromBody]string messageBody)
        {
            _logger = new Logger(this.GetType().Name);
            _logger.Log(Loglevel.Trace, string.Empty, "0", "Item http PUT request received.", messageBody);

            return await SendRequestToBackend(PrepareAndTransformMessage(messageBody, "Update Item"));

        }

        private string PrepareAndTransformMessage(string messageBody, string requestType)
        {

            string transactionId = Guid.NewGuid().ToString();

            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml(messageBody);

            XmlElement _rentaldb = xdoc.CreateElement("RentalDBName");
            _rentaldb.InnerText = System.Configuration.ConfigurationManager.AppSettings["RentalDBName"];

            XmlElement _transactionId = xdoc.CreateElement("TransactionId");
            _logkey = Guid.NewGuid().ToString();
            _transactionId.InnerText = _logkey;

            XmlElement _creationDateTime = xdoc.CreateElement("CreationDateTime");
            _creationDateTime.InnerText = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss+0100");

            XmlElement _requestType = xdoc.CreateElement("RequestType");
            _requestType.InnerText = requestType;

            XmlElement root = xdoc.DocumentElement;

            root.InsertAfter(_rentaldb, root.LastChild);
            root.InsertAfter(_transactionId, root.LastChild);
            root.InsertAfter(_creationDateTime, root.LastChild);
            root.InsertAfter(_requestType, root.LastChild);

            _logger.Log(Loglevel.Trace, _logkey, "10", "Message prepared.", xdoc.OuterXml);

            Transformer t = new Transformer("cramo_cra3000_items.resources.PIMItem_SyncItemMaster.xslt", System.Text.Encoding.UTF8, true, true);

            string _transformResult = t.Transform(xdoc.OuterXml);

            _logger.Log(Loglevel.Trace, _logkey, "20", "Message transformed.", _transformResult);

            return _transformResult;

        }




        private async Task<HttpResponseMessage> SendRequestToBackend(string messageBody)
        {
            using (var client = new HttpClient())
            {

                string baseAddress = System.Configuration.ConfigurationManager.AppSettings["360.BaseAddress"];
                string uri = System.Configuration.ConfigurationManager.AppSettings["360.Uri.Items"];
                string subscriptionkey = System.Configuration.ConfigurationManager.AppSettings["360.SubscriptionKey"];

                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/xml"));

                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", subscriptionkey);

                Console.Out.WriteLine("Sending request to 360");

                HttpResponseMessage response = await client.PostAsync(uri, new StringContent(messageBody));

                _logger.Log(Loglevel.Trace, string.Empty, "30", "Items request sent to Rental backend. Response attached.", string.Format("Http status: {0}, http response: {1}.", response.StatusCode.ToString(), await response.Content.ReadAsStringAsync()));

                return response;

            }

        }
    }
}
