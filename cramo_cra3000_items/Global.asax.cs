﻿using cramo_cra3000_items.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace cramo_cra3000_items
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            GlobalConfiguration.Configuration.Formatters.Insert(0, new TextMediaTypeFormatter());

            PriceManager.Initialize();

        }
    }
}
