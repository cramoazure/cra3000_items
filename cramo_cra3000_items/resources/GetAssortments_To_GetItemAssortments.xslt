﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:output method="xml" indent="yes"/>
  <xsl:template match="GetAssortments">
    <cramo:GetItemAssortments xmlns:cramo="http://www.cramo.com/canonical" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cramo.com/canonical ../Schemas_Extension/GetItemAvailabilitys.xsd ">
      <cramo:ApplicationArea>
        <cramo:Sender>
          <cramo:ReferenceID>
            <xsl:value-of select="MarketId"/>
          </cramo:ReferenceID>
        </cramo:Sender>
        <cramo:CreationDateTime>
          <xsl:value-of select="CreationDateTime"/>
        </cramo:CreationDateTime>
        <cramo:BODID>
          <xsl:value-of select="NewTransactionId"/>
        </cramo:BODID>
      </cramo:ApplicationArea>
      <cramo:DataArea>
        <xsl:for-each select="Items/Item">
          <cramo:ItemAssortmentRequest>
            <cramo:Item>
              <xsl:value-of select="ItemNo" />
            </cramo:Item>
            <cramo:AssortmentID />
          </cramo:ItemAssortmentRequest>
        </xsl:for-each>
      </cramo:DataArea>
    </cramo:GetItemAssortments>
  </xsl:template>
</xsl:stylesheet>
