﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:ns="http://www.cramo.com/canonical" exclude-result-prefixes="msxsl"
>
  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="ns:ShowItemAvailabilitys">
    <ShowAvailability>
      <MarketId>
        <xsl:value-of select="ns:ApplicationArea/ns:Sender/ns:ReferenceID"/>
      </MarketId>
      <TransactionId>
        <xsl:value-of select ="ns:ApplicationArea/ns:BODID"/>
      </TransactionId>
      <Items>
      <xsl:for-each select="ns:DataArea/ns:ItemAvailability">
        <Item>
          <ItemNo>
            <xsl:value-of select="ns:Item"/>
          </ItemNo>
          <xsl:choose>
            <xsl:when test="ns:Error">
              <Error>
                <ErrorCode>
                  <xsl:value-of select="ns:Error/ns:ErrorCode"/>
                </ErrorCode>
                  
                <ErrorText>
                  <xsl:value-of select="ns:Error/ns:ErrorText"/>
                </ErrorText>
            </Error>
            
          </xsl:when>
          <xsl:otherwise>
          
            <TotalQuantityAvailable>
            <xsl:value-of select="ns:QuantityAvailable"/>
          </TotalQuantityAvailable>
          <TotalQuantityPossibleAvailableNoReturn>
            <xsl:value-of select="ns:QuantityPossibleAvailableNoReturn"/>
          </TotalQuantityPossibleAvailableNoReturn>
          <Unit>
            <xsl:value-of select="ns:Unit"/>
          </Unit>
          <xsl:variable name="date" select="ns:DateRequested"/>
          <RequestDate>
            <xsl:value-of select="translate(substring($date,1,10),'-','')"/>
          </RequestDate>
          <xsl:for-each select="ns:OrganizationalUnits">
            <Depots>
              <Depot>
                <xsl:value-of select="ns:OrganizationalUnit"/>
              </Depot>
              <QuantityAvailable>
                <xsl:value-of select="ns:QuantityAvailable"/>
              </QuantityAvailable>
              <QuantityPossibleAvailableNoReturn>
                <xsl:value-of select="ns:QuantityPossibleAvailableNoReturn"/>
              </QuantityPossibleAvailableNoReturn>
            </Depots>
          
          </xsl:for-each>
          </xsl:otherwise>
          </xsl:choose>
          
        </Item>
      </xsl:for-each>
      </Items>
    </ShowAvailability>
  </xsl:template>
</xsl:stylesheet>
