﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:output method="xml" indent="yes"/>
  <xsl:template match="GetAvailability">

    <xsl:variable name="datetoday" select="CreationDateTime"/>

    <cramo:GetItemAvailabilitys xmlns:cramo="http://www.cramo.com/canonical" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cramo.com/canonical ../Schemas_Extension/GetItemAvailabilitys.xsd ">
      <cramo:ApplicationArea>
        <cramo:Sender>
          <cramo:ReferenceID>
            <xsl:value-of select="MarketId"/>
          </cramo:ReferenceID>
        </cramo:Sender>
        <cramo:CreationDateTime>
          <xsl:value-of select="$datetoday"/>
        </cramo:CreationDateTime>
        <cramo:BODID>
          <xsl:value-of select="NewTransactionId"/>
        </cramo:BODID>
      </cramo:ApplicationArea>
      <cramo:DataArea>
        <xsl:for-each select="Items/Item">
          <cramo:ItemAvailabilityRequest>
            <cramo:Item>
              <xsl:value-of select="ItemNo" />
            </cramo:Item>
            <!-- Quantity, default to 1 if not provided -->
            <xsl:variable name="quantity" select="Quantity"/>
            <xsl:choose>
              <xsl:when test="$quantity != ''">
                <cramo:QuantityRequested>
                  <xsl:value-of select="$quantity"/>
                </cramo:QuantityRequested>
              </xsl:when>
              <xsl:otherwise>
                <cramo:QuantityRequested>
                  <xsl:value-of select="1"/>
                </cramo:QuantityRequested>
              </xsl:otherwise>
            </xsl:choose>
            <!-- RequestDate, default to today if not provided -->
            <xsl:variable name="date" select="RequestDate"/>
            <xsl:choose>
              <xsl:when test="$date != '' and string-length($date) = 8">
                <cramo:DateRequested>
                  <xsl:value-of select="concat(substring($date, 1, 4), '-', substring($date, 5, 2), '-', substring($date, 7, 2))"/>
                </cramo:DateRequested>
              </xsl:when>
              <xsl:otherwise>
                <cramo:DateRequested>
                  <xsl:value-of select="substring($datetoday,1,10)"/>
                </cramo:DateRequested>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:variable name="unit" select="Unit"/>
            <xsl:if test="$unit!=''">
              <cramo:Unit>
                <xsl:value-of select="$unit" />
              </cramo:Unit>
            </xsl:if>
            <xsl:for-each select="Depots/Depot">
              <cramo:OrganizationalUnit>
                <xsl:value-of select="."/>
              </cramo:OrganizationalUnit>
            </xsl:for-each>
          </cramo:ItemAvailabilityRequest>
        </xsl:for-each>
      </cramo:DataArea>
    </cramo:GetItemAvailabilitys>
  </xsl:template>
</xsl:stylesheet>
