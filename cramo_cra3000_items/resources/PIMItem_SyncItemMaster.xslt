<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output encoding="UTF-8" indent="yes" method="xml"/>
	<xsl:template match="/Item">
		<SyncItemMaster xmlns="http://www.openapplications.org/oagis/10" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" releaseID="1">
			<ApplicationArea>
				<Sender>
					<ComponentID>PIM</ComponentID>
					<TaskID>
						<xsl:value-of select="./RequestType"/>
					</TaskID>
					<ReferenceID>
						<xsl:value-of select="./RentalDBName"/>
					</ReferenceID>
				</Sender>
				<CreationDateTime>
					<xsl:value-of select="./CreationDateTime"/>
				</CreationDateTime>
				<BODID>
					<xsl:value-of select="./TransactionId"/>
				</BODID>
			</ApplicationArea>
			<DataArea>
				<Sync/>
				<!--xsl:for-each select="/Item"-->
				<ItemMaster>
					<ID>
						<xsl:value-of select="./ItemNumber"/>
					</ID>
					<ItemIDSet>
						<ID>
							<xsl:value-of select="./ItemERPTemplate/key/@id"/>
						</ID>
					</ItemIDSet>
					<!-- Description, Max 50 chars -->
					<xsl:for-each select="./ItemDescriptionShort/value">
						<xsl:if test="current() != '' ">
							<!-- Also create system language for en and sv - to CEN and CSV -->
							<xsl:if test="current()[@language='en']">
								<Description>
									<xsl:attribute name="languageCode">
										<xsl:text>CEN</xsl:text>
									</xsl:attribute>
									<xsl:value-of select="substring(current(), 1, 50)"/>
								</Description>
							</xsl:if>
							<xsl:if test="current()[@language='sv']">
								<Description>
									<xsl:attribute name="languageCode">
										<xsl:text>CSV</xsl:text>
									</xsl:attribute>
									<xsl:value-of select="substring(current(), 1, 50)"/>
								</Description>
							</xsl:if>
							<!-- Actual language code, keep as is -->
							<Description>
								<xsl:attribute name="languageCode">
									<!--xsl:value-of select="@language"/-->
									<xsl:value-of select="translate(@language, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
								</xsl:attribute>
								<xsl:value-of select="substring(current(), 1, 50)"/>
							</Description>
						</xsl:if>
					</xsl:for-each>
					<!-- Notes, full description -->
					<xsl:for-each select="./ItemDescriptionShort/value">
						<xsl:if test="current() != '' ">
							<!-- Also create system language for en and sv - to CEN and CSV -->
							<xsl:if test="current()[@language='en']">
								<Note>
									<xsl:attribute name="languageCode">
										<xsl:text>CEN</xsl:text>
									</xsl:attribute>
									<xsl:value-of select="current()"/>
								</Note>
							</xsl:if>
							<xsl:if test="current()[@language='sv']">
								<Note>
									<xsl:attribute name="languageCode">
										<xsl:text>CSV</xsl:text>
									</xsl:attribute>
									<xsl:value-of select="current()"/>
								</Note>
							</xsl:if>
							<!-- Actual language code, keep as is -->
							<Note>
								<xsl:attribute name="languageCode">
									<!--xsl:value-of select="@language"/-->
									<xsl:value-of select="translate(@language, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
								</xsl:attribute>
								<xsl:value-of select="current()"/>
							</Note>
						</xsl:if>
					</xsl:for-each>
				</ItemMaster>
				<!--/xsl:for-each-->
			</DataArea>
		</SyncItemMaster>
	</xsl:template>
</xsl:stylesheet>
