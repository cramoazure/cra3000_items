﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:ns="http://www.cramo.com/canonical" exclude-result-prefixes="msxsl"
>
  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="ns:ShowItemAvailabilitys">
    <ShowAvailability>
      <MarketId>
        <xsl:value-of select="ns:ApplicationArea/ns:Sender/ns:ReferenceID"/>
      </MarketId>
      <TransactionId>
        <xsl:value-of select ="ns:ApplicationArea/ns:BODID"/>
      </TransactionId>
      <Items>
      <xsl:for-each select="ns:DataArea/ns:ItemAvailability">
        <Item>
          <ItemNo>
            <xsl:value-of select="ns:Item"/>
          </ItemNo>
          <TotalQuantityAvailable>
            <xsl:value-of select="ns:QuantityAvailable"/>
          </TotalQuantityAvailable>
          <TotalEstimatedQuantityAvailable>
            <xsl:value-of select="ns:QuantityPossibleAvailableNoReturn"/>
          </TotalEstimatedQuantityAvailable>
          <Unit>
            <xsl:value-of select="ns:Unit"/>
          </Unit>
          <xsl:variable name="date" select="ns:DateRequested"/>
          <RequestDate>
            <xsl:value-of select="translate(substring($date,1,10),'-','')"/>
          </RequestDate>
          <xsl:for-each select="ns:OrganizationalUnits">
            <Depots>
              <Depot>
                <xsl:value-of select="ns:OrganizationalUnit"/>
              </Depot>
              <QuantityAvailable>
                <xsl:value-of select="ns:QuantityAvailable"/>
              </QuantityAvailable>
              <EstimatedQuantityAvailable>
                <xsl:value-of select="ns:QuantityPossibleAvailableNoReturn"/>
              </EstimatedQuantityAvailable>
            </Depots>
          </xsl:for-each>
        </Item>
      </xsl:for-each>
      </Items>
    </ShowAvailability>
  </xsl:template>
</xsl:stylesheet>
