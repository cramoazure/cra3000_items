﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:ns="http://www.openapplications.org/oagis/10" exclude-result-prefixes="msxsl ns"
>
  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="ns:SyncItemMaster">
    <ShowAssortments>
      <MarketId>
        <xsl:value-of select="ns:ApplicationArea/ns:Sender/ns:ReferenceID"/>
      </MarketId>
      <TransactionId>
        <xsl:value-of select="ns:ApplicationArea/ns:BODID"/>
      </TransactionId>
      <Items>
        <xsl:for-each select="ns:DataArea/ns:ItemMaster">
          <Item>
            <ItemNo>
              <xsl:value-of select="ns:ID"/>
            </ItemNo>
            <ItemDescription>
              <xsl:value-of select="ns:Description"/>
            </ItemDescription>
            <Assortments>
              <xsl:for-each select="ns:DocumentReference">
                <Assortment>
                  <ItemNo>
                    <xsl:value-of select="ns:DocumentIDSet/ns:ID" />
                  </ItemNo>
                  <ItemDescription>
                    <xsl:value-of select="ns:Description"/>
                  </ItemDescription>
                  <Quantity>
                    <xsl:value-of select="ns:Extension/ns:Quantity"/>      
                  </Quantity>
                  <!--<BrandType></BrandType>-->
                </Assortment>
              </xsl:for-each>
            </Assortments>
          </Item>
        </xsl:for-each>
      </Items>
    </ShowAssortments>
  </xsl:template>
</xsl:stylesheet>
