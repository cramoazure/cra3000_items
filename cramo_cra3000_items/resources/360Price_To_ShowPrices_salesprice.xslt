﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:cramo="http://www.cramo.com/canonical" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="cramo" >
  <xsl:output encoding="UTF-8" indent="yes" method="xml"/>
  <xsl:template match="@* | node()">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="/">
    <ShowPrices>
      <MarketId>
        <xsl:value-of select="//cramo:ApplicationArea/cramo:Sender/cramo:ReferenceID"/>
      </MarketId>
      <TransactionId>
        <xsl:value-of select="//cramo:BODID"/>
      </TransactionId>
      <Prices>
        <!-- ShowPrices -->
        <xsl:for-each select="//cramo:Price">
          <Price>
            <Item>
              <xsl:value-of select="./cramo:Item"/>
            </Item>
            <Quantity>
              <xsl:value-of select="./cramo:Quantity"/>
            </Quantity>
            <Customer>
              <xsl:value-of select="./cramo:Customer"/>
            </Customer>
            <xsl:apply-templates select="./cramo:PriceFromDate"/>
            <Currency>
              <xsl:value-of select="./cramo:Currency"/>
            </Currency>
            <OrganizationalUnit>
              <xsl:value-of select="./cramo:OrganizationalUnit"/>
            </OrganizationalUnit>
            <OrderType>
              <xsl:value-of select="./cramo:OrderType"/>
            </OrderType>
            <UnitPrice>
              <xsl:value-of select="./cramo:UnitPrice"/>
            </UnitPrice>
            <VATpercentage>
              <xsl:value-of select="./cramo:VATpercentage"/>
            </VATpercentage>
            <Unit>
              <xsl:value-of select="./cramo:Unit"/>
            </Unit>
            <Warehouse>
              <xsl:value-of select="./cramo:Warehouse"/>
            </Warehouse>
            <PriceList>
              <xsl:value-of select="./cramo:PriceList"/>
            </PriceList>
            <NetPrice>
              <xsl:value-of select="./cramo:NetPrice"/>
            </NetPrice>
            <Discount>
              <xsl:value-of select="./cramo:Discount"/>
            </Discount>
            <xsl:for-each select="./cramo:Error">
              <xsl:copy>
                <xsl:apply-templates select="@* | node()"/>
              </xsl:copy>
            </xsl:for-each>
          </Price>
        </xsl:for-each>
      </Prices>
    </ShowPrices>
  </xsl:template>
  <xsl:template match="cramo:PriceFromDate">
    <xsl:variable name="strLength">
      <xsl:value-of select="string-length(.)" />
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$strLength &gt; 10">
        <PriceFromDate>
          <xsl:value-of select="substring(., 1, 10)" />
        </PriceFromDate>
      </xsl:when>
      <xsl:otherwise>
        <!-- Keep as is -->
        <PriceFromDate>
          <xsl:value-of select="." />
        </PriceFromDate>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>