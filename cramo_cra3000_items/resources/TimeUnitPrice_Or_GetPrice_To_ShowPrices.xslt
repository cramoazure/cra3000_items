﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns="http://www.cramo.com/canonical" xmlns:cramo="http://www.cramo.com/canonical" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output encoding="UTF-8" exclude-result-prefixes="cramo" indent="yes" method="xml"/>
  <xsl:template match="@* | node()">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="/">
    <ShowPrices>
      <xsl:attribute name="versionID">
        <xsl:text>1</xsl:text>
      </xsl:attribute>
      <MarketId>
        <xsl:value-of select="cramo:ShowPrices/cramo:ApplicationArea/cramo:Sender/cramo:ReferenceID"/>
      </MarketId>
      <TransactionId>
        <xsl:value-of select="//cramo:BODID"/>
      </TransactionId>
      <Prices>
        <!-- GetTimeUnitPrice -->
        <xsl:for-each select="//cramo:TimeUnitPrice">
          <Price>
            <xsl:apply-templates select="@* | node()"/>
          </Price>
        </xsl:for-each>
        <!-- ShowPrices -->
        <xsl:for-each select="//cramo:Price">
          <Price>
            <xsl:apply-templates select="@* | node()"/>
          </Price>
        </xsl:for-each>
      </Prices>
    </ShowPrices>
  </xsl:template>
  <xsl:template match="cramo:PriceFromDate">
    <xsl:variable name="strLength">
      <xsl:value-of select="string-length(.)" />
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$strLength &gt; 10">
        <PriceFromDate>
          <xsl:value-of select="substring(., 1, 10)" />
        </PriceFromDate>
      </xsl:when>
      <xsl:otherwise>
        <!-- Keep as is -->
        <PriceFromDate>
          <xsl:value-of select="." />
        </PriceFromDate>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="cramo:PriceToDate">
    <xsl:variable name="strLength">
      <xsl:value-of select="string-length(.)" />
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$strLength &gt; 10">
        <PriceToDate>
          <xsl:value-of select="substring(., 1, 10)" />
        </PriceToDate>
      </xsl:when>
      <xsl:otherwise>
        <!-- Keep as is -->
        <PriceToDate>
          <xsl:value-of select="." />
        </PriceToDate>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>