﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
    <xsl:output method="xml" indent="yes"/>

  <xsl:template match="GetPrices">

        <cramo:GetPrices xmlns:cramo="http://www.cramo.com/canonical">
          <cramo:ApplicationArea>
            <cramo:Sender>
              <cramo:ComponentID>EPiServer</cramo:ComponentID>
              <cramo:TaskID>Price request</cramo:TaskID>
              <cramo:ReferenceID>
                <xsl:value-of select ="MarketId"/>
              </cramo:ReferenceID>
            </cramo:Sender>
            <cramo:CreationDateTime>
              <xsl:value-of select="CreationDateTime"/>
            </cramo:CreationDateTime>
            <cramo:BODID>
              <xsl:value-of select="NewTransactionId"/>
            </cramo:BODID>
          </cramo:ApplicationArea>
          <cramo:DataArea>
            <xsl:for-each select="Items/Item">
              <cramo:PriceRequest>
                <cramo:OrderType>XR</cramo:OrderType>
                <cramo:Item>
                  <xsl:value-of select="ItemNo"/>
                </cramo:Item>
                <cramo:Quantity>
                  <xsl:variable name="quantity" select="Quantity" />
                  <!-- Set 1 if not provided -->
                  <xsl:choose>
                  <xsl:when test="string-length($quantity) > 0">
                    <xsl:value-of select="Quantity"/>    
                  </xsl:when>
                    <xsl:otherwise>
                      <xsl:text>1</xsl:text>
                    </xsl:otherwise>
                  </xsl:choose>
                </cramo:Quantity>
                <cramo:Customer>
                  <xsl:value-of select="CustomerId"/>
                </cramo:Customer>
                <cramo:OrganizationalUnit>
                  <xsl:value-of select="DepotId"/>
                </cramo:OrganizationalUnit>
                <cramo:PriceFromDate>
                  <xsl:value-of select="FromDate"/>
                </cramo:PriceFromDate>
              </cramo:PriceRequest>
            </xsl:for-each>
          </cramo:DataArea>
        </cramo:GetPrices>

  </xsl:template>


</xsl:stylesheet>
