﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.ServiceBus.Messaging;
using System.Net.Http;
using System.Net.Http.Headers;
using Cramo.Utils.Transformations;
using Cramo.Utils.Logging;
using System.Threading;
using System.Xml.Linq;
using System.Diagnostics;
using SendGrid;
using Microsoft.Azure.WebJobs.Extensions;
using System.Net.Mail;
using Microsoft.Azure.WebJobs.Host;
using System.Configuration;
using Microsoft.ApplicationInsights;
using System.IO.Compression;
using cramo_cra3000_webjobs.Resources;
using Microsoft.WindowsAzure.Storage;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage.Blob;

namespace cramo_cra3000_webjobs
{
    public class Functions
    {
        private static Logger _logger;

        private static TelemetryClient telemetry;

        public const string QueueName = "items";
        public const string PriceListTopicName = "pricelists";

        // This function will get triggered/executed when a new message is written 
        // on an Azure Queue called queue.
        public static async void ProcessItemsTopicMessage([ServiceBusTrigger(QueueName, "pim")] BrokeredMessage message, TraceWriter trace)
        {

            Trace.TraceInformation($"New message detected on topic '{QueueName}' for subscriber 'pim' with messageId {message.MessageId}. DeliveryCount: {message.DeliveryCount.ToString()}.");
            //Console.WriteLine(string.Format("Received message: {0}, {1}", message.MessageId, message.DeliveryCount.ToString()));

            _logger = new Logger("ProcessItemsTopicMessage");
            string logkey = Guid.NewGuid().ToString();

            string itemBody = message.GetBody<string>();

            Trace.TraceInformation($"Message: \n {itemBody}.");

            _logger.Log(Loglevel.Trace, logkey, "10", $"New item message on topic {QueueName} for subscriber 'pim' with messageId ({message.MessageId}).", itemBody);

            XDocument xdoc = XDocument.Load(new StringReader(itemBody));
            XNamespace ns = "http://www.openapplications.org/oagis/10";

            //XElement bodid = (from xml in xdoc.Descendants(ns + "BODID")
            //                      select xml).FirstOrDefault();


            XElement market = (from xml in xdoc.Descendants(ns + "ReferenceID")
                               select xml).FirstOrDefault();

            string marketId = market.Value;


            ///SyncItemMaster/DataArea/ItemMaster/ID[@typeCode='ItemNumber']
            XElement item = (from xml in xdoc.Descendants(ns + "ID")
                             where xml.Attribute("typeCode").Value == "ItemNumber"
                             select xml).FirstOrDefault();

            string itemNumber = item.Value;

            // Transform
            Trace.TraceInformation($"Transforming message with itemNumber '{itemNumber}' from market '{marketId}'.");

            Transformer t = new Transformer("cramo_cra3000_webjobs.Resources.SyncItemMaster_To_ItemRentalAttributeUpdate.xslt", Encoding.UTF8, true, false);

            string itemRentalAttributeUpdate = t.Transform(itemBody);

            //_logger.Log(Loglevel.Trace, logkey, "20", "Message transformed.", itemRentalAttributeUpdate);

            // Send

            HttpResponseMessage response = SubmitItemUpdateToPIM(itemRentalAttributeUpdate).Result;

            if (response.IsSuccessStatusCode)
            {
                Trace.TraceInformation($"PIM says {response.StatusCode.ToString()}");
                Trace.TraceInformation($"PIM says {await response.Content.ReadAsStringAsync()}");

                _logger.Log(Loglevel.Trace, logkey, "20", $"Message sent successfully to PIM. PIM says {response.StatusCode.ToString()}: {await response.Content.ReadAsStringAsync()}", itemRentalAttributeUpdate);

                message.Complete();
                Trace.TraceInformation("Message completed.");

            }
            else
            {
                Trace.TraceError($"PIM returned non-successful http code.");
                _logger.Log(Loglevel.Error, logkey, "900", $"Failed to send message. PIM says {response.StatusCode.ToString()}: {await response.Content.ReadAsStringAsync()}", itemRentalAttributeUpdate);

                string errormessage = $"PIM returned non-succesful http code '{response.StatusCode.ToString()}'. PIM says: {response.Content.ReadAsStringAsync().Result}. \n\n\b Market: {marketId} \n\b Item: {itemNumber}";

                TraceEvent error = new TraceEvent(TraceLevel.Error, $"An error has occurred while sending item '{itemNumber}' in market '{marketId}' to PIM.");
                error.Properties.Add("HttpStatusCode", response.StatusCode.ToString());
                error.Properties.Add("HttpResponse", response.Content.ReadAsStringAsync().Result);
                error.Properties.Add("Market", marketId);
                error.Properties.Add("Item", itemNumber);
                trace.Trace(error);


                if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                {
                    // PIM returns 500 - try again. After 10 attempts message is moved to deadletter queue.

                    Trace.TraceError($"PIM says {response.StatusCode.ToString()}");
                    Trace.TraceError($"PIM says {await response.Content.ReadAsStringAsync()}");

                    message.Abandon();
                    Trace.TraceError("Message abandoned.");
                    //Thread.Sleep(TimeSpan.FromSeconds(30));
                }
                else
                {
                    // if not InternalServerError means either message is delivered successfully, or message was malformed -> no reason to try to send again.

                    Trace.TraceWarning($"PIM says {response.StatusCode.ToString()}");
                    Trace.TraceWarning($"PIM says {await response.Content.ReadAsStringAsync()}");


                    message.Complete();
                    Trace.TraceWarning("Message completed.");
                }

                //throw new Exception(errormessage);                

            }


        }

        public static void ProcessPriceListTopicMessage([ServiceBusTrigger(PriceListTopicName, "erp")] BrokeredMessage message, TraceWriter trace)
        {
            Trace.TraceInformation("New message found for subscription 'erp' on topic '" + PriceListTopicName + "'");

            var filepath = Path.GetTempFileName();

            using (var filestream = File.OpenWrite(filepath))
            {
                GetBody(message, filestream);

            }

            // Time consuming and peek lock times out. 
            message.Complete();
            Trace.TraceInformation("Temporary Excelfile written to: " + filepath);

            // Create XML
            PriceList priceListHandler = new PriceList();
            priceListHandler.Parse(filepath);

            // Post to 360
            bool sentOk = priceListHandler.Send();

            if (sentOk)
            {
                //message.Complete();
                Trace.TraceInformation("Message completed.");
            } else
            {
                //message.Abandon();
                Trace.TraceError("Message could not be sent to 360.");
            }
            

        }

        private static void GetBody(BrokeredMessage message, Stream s)
        {

            if (message.Properties["Blob"].ToString() == "true")
            {
                string blobReference = message.GetBody<string>();

                ReadBlob(blobReference, s);

            }
            else
            {
                throw new ArgumentException("Message is not a blob message");
            }

        }

        private static void ReadBlob(string blobReference, Stream s)
        {

            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("Storage.ConnectionString"));

            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            CloudBlobContainer container = blobClient.GetContainerReference("topiccontainer");

            CloudBlockBlob messageBlob = container.GetBlockBlobReference(blobReference);

            messageBlob.DownloadToStream(s);

            messageBlob.Delete();


        }

        public static void ProcessItemsTopicMessageErrorHandler([ErrorTrigger()] Microsoft.Azure.WebJobs.Extensions.TraceFilter filter)
        {
            Trace.TraceInformation("Entering Errorhandler");

            var ev = filter.Events.First();

            string body;


            try
            {
                body = $"An error has occurred while processing an item update message going to PIM. \n\n PIM says {(string)ev.Properties["HttpStatusCode"]}: {(string)ev.Properties["HttpResponse"]} \n\n Itemnumber: '{(string)ev.Properties["Item"]}' in market '{(string)ev.Properties["Market"]}'.";
            }
            catch (Exception ex)
            {
                body = ev.ToString();
            }

            Trace.TraceInformation(body);

            // create the message

            telemetry = new TelemetryClient();

            try
            {
                telemetry.TrackEvent("Item update failed", new Dictionary<string, string> { { "Response", (string)ev.Properties["HttpResponse"] }, { "Item", (string)ev.Properties["Item"] }, { "Market", (string)ev.Properties["Market"] } });
            }
            catch (Exception ex)
            {
                //telemetry.TrackEvent
                Trace.TraceError(ex.ToString());
            }

            string smtpServer = ConfigurationManager.AppSettings["SmtpServer"];

            if (!string.IsNullOrEmpty(smtpServer))
            {
                Trace.TraceInformation("Found email settings.");

                var emailMessage = new MailMessage();

                emailMessage.From = new MailAddress(ConfigurationManager.AppSettings["SmtpUser"]);
                emailMessage.Sender = emailMessage.From;

                string mailAddresses = ConfigurationManager.AppSettings["NotifyEmail"];

                foreach (string address in mailAddresses.Split(new char[] { ';', ' ' }))
                {
                    emailMessage.To.Add(new MailAddress(address));
                }

                emailMessage.Subject = filter.Message;
                emailMessage.IsBodyHtml = false;
                emailMessage.Body = body;

                var smtp = new SmtpClient(smtpServer);
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SmtpUser"], ConfigurationManager.AppSettings["SmtpPassword"]);
                smtp.Port = 587;
                smtp.EnableSsl = true;

                Trace.TraceInformation($"Email will be sent using server '{smtpServer}' to '{mailAddresses}' with subject {emailMessage.Subject}");

                // send the message
                smtp.Send(emailMessage);

            }

        }

        private static async Task<HttpResponseMessage> SubmitItemUpdateToPIM(string messageBody)
        {
            using (var client = new HttpClient())
            {

                string baseAddress = System.Configuration.ConfigurationManager.AppSettings["PIM.BaseAddress"];
                string uri = System.Configuration.ConfigurationManager.AppSettings["PIM.Uri.ItemUpdate"];
                string subscriptionkey = System.Configuration.ConfigurationManager.AppSettings["PIM.SubscriptionKey"];

                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/xml"));

                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", subscriptionkey);

                //Console.Out.WriteLine(string.Format("Sending request to PIM: {0}", messageBody));

                Trace.TraceInformation($"Sending POST {client.BaseAddress}{uri}");

                return await client.PostAsync(uri, new StringContent(messageBody));

            }

        }

        private static byte[] Unzip(byte[] bytes)
        {
            using (var msi = new MemoryStream(bytes))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(msi, CompressionMode.Decompress))
                {
                    CopyTo(gs, mso);
                }

                return mso.ToArray();
            }
        }

        private static void CopyTo(Stream src, Stream dest)
        {
            byte[] bytes = new byte[4096];

            int cnt;

            while ((cnt = src.Read(bytes, 0, bytes.Length)) != 0)
            {
                dest.Write(bytes, 0, cnt);
            }
        }

    }
}
