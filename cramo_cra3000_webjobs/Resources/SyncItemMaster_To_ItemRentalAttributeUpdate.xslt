﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ns0="http://www.openapplications.org/oagis/10"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl ns0"
>
    <xsl:output method="xml" indent="yes" encoding="utf-8"/>

    <xsl:template match="/">

      <ItemRentalAttributeUpdate>
        <ItemNumber>
          <xsl:value-of select="//ns0:ItemMaster/ns0:ID[@typeCode='ItemNumber']"/>
        </ItemNumber>
        <Attributes>
          <xsl:element name="Attribute">
            <xsl:attribute name="MarketCode">
              <xsl:value-of select="/ns0:SyncItemMaster/ns0:ApplicationArea/ns0:Sender/ns0:ReferenceID"/>
            </xsl:attribute>
            <xsl:attribute name="Id">IsActive</xsl:attribute>
              <xsl:choose>
                <xsl:when test="//ns0:ItemMaster/ns0:ItemStatus/ns0:Code[@typeCode='StatusCode']='D'">
                  <xsl:text>false</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:text>true</xsl:text>
                </xsl:otherwise>
              </xsl:choose>
            
          </xsl:element>
        </Attributes>
      </ItemRentalAttributeUpdate>
    
    </xsl:template>
</xsl:stylesheet>
