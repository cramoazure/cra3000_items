﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace cramo_cra3000_webjobs.Resources
{
    class PriceList
    {

        private XmlDocument xmlDoc;
        private string dataSheetName = "Update data";
        private string GUID = "";
        private int modularTraceValue = 100;
        private Dictionary<string, string> parameters;
        private TelemetryClient telemetry;

        private String MarketId { get; set; }
        private String PriceListName { get; set; }
        private String Currency { get; set; }
        private String DateFormat { get; set; }
        private List<ListRecord> Lines { get; set; }


        public PriceList()
        {
            xmlDoc = new XmlDocument();
            GUID = Guid.NewGuid().ToString();
            Lines = new List<ListRecord>();
            parameters = new Dictionary<string, string>();

            telemetry = new TelemetryClient();
            telemetry.TrackTrace("Pricelist in Excel received for price list update in ERP/Rental.", SeverityLevel.Information, parameters);

            //Parse(fileName);

        }

        public void Parse(string fileName)
        {
            Trace.TraceInformation("Opening Excel file from: " + fileName);
            GetCellValues(fileName, dataSheetName);

            Trace.TraceInformation("Creating XML...");
            CreateXML();

            Trace.TraceInformation(xmlDoc.OuterXml.ToString());

        }

        private void GetCellValues(string fileName, string sheetName)
        {
            //string value = null;

            // Open the spreadsheet document for read-only access.
            using (SpreadsheetDocument document =
                SpreadsheetDocument.Open(fileName, false))
            {
                // Retrieve a reference to the workbook part.
                WorkbookPart wbPart = document.WorkbookPart;

                // Find the sheet with the supplied name, and then use that 
                // Sheet object to retrieve a reference to the first worksheet.
                Sheet theSheet = wbPart.Workbook.Descendants<Sheet>().
                  Where(s => s.Name == sheetName).FirstOrDefault();

                // Throw an exception if there is no sheet.
                if (theSheet == null)
                {
                    throw new ArgumentException("sheetName");
                }

                // Retrieve a reference to the worksheet part.
                WorksheetPart wsPart =
                    (WorksheetPart)(wbPart.GetPartById(theSheet.Id));


                // Get the given cells
                MarketId = GetLocalCellValue(wbPart, wsPart, "B9");
                PriceListName = GetLocalCellValue(wbPart, wsPart, "B10");
                Currency = GetLocalCellValue(wbPart, wsPart, "B11");
                DateFormat = GetLocalCellValue(wbPart, wsPart, "B12");

                if (DateFormat != null || DateFormat.TrimEnd().Equals(""))
                {
                    // Default the value
                    DateFormat = "yyyyMMdd";
                }

                Trace.TraceInformation("Basic settings read -> Market: {0}, PriceList: {1}, Currency: {2}, DateFormat: {3}",
                    MarketId, PriceListName, Currency, DateFormat);

                parameters.Add("Market", MarketId);
                parameters.Add("PriceList", PriceListName);
                parameters.Add("Currency", Currency);
                parameters.Add("DateFormat", DateFormat);

                // Loop Lines
                string strItemNumber = "";
                string strUnit = "";
                string strNewPrice = "";
                string strTimeUnit = "";
                string strFromDate = "";
                string strNewDate = "";
                int nrOfRows = 0;
                ListRecord currentLine = null;

                for (int i = 16; i < 19999; i++)
                {
                    strItemNumber = GetLocalCellValue(wbPart, wsPart, "B" + i);
                    strUnit = GetLocalCellValue(wbPart, wsPart, "C" + i);
                    strNewPrice = GetLocalCellValue(wbPart, wsPart, "D" + i);
                    strTimeUnit = GetLocalCellValue(wbPart, wsPart, "E" + i);
                    strFromDate = GetLocalCellValue(wbPart, wsPart, "F" + i);

                    // Check if line item exists
                    if (strItemNumber == null || strItemNumber.TrimEnd().Equals(""))
                    {
                        Trace.TraceInformation("END of Data. Exiting the reading of lines at line {0}. Total nr of lines parsed: {1}", i.ToString(), nrOfRows);
                        break;
                    }

                    // Format and adjust values
                    strNewPrice = strNewPrice.Replace(',', '.');
                    strNewDate = FormatDateToYMD8(strFromDate, DateFormat);

                    // Price handling
                    decimal price;
                    if (Decimal.TryParse(strNewPrice, out price))
                    {
                        strNewPrice = price.ToString("0.####");
                        //Trace.TraceInformation("Price converted and adjusted: " + strNewPrice);
                    }
                    else
                    {
                        Trace.TraceInformation("not a Decimal... -> " + strNewPrice);
                    }

                    currentLine = new ListRecord();
                    currentLine.ItemNumber = strItemNumber.TrimEnd();
                    currentLine.Unit = strUnit.TrimEnd();
                    currentLine.NewPrice = strNewPrice.TrimEnd();
                    currentLine.TimeUnit = strTimeUnit.TrimEnd();
                    currentLine.ValidFromDate = strFromDate.TrimEnd();
                    currentLine.FormattedValidFromDate = strNewDate.TrimEnd();

                    Lines.Add(currentLine);

                    nrOfRows++;
                    int modValue = nrOfRows % modularTraceValue;

                    if (modValue == 0)
                    {
                        Trace.TraceInformation("Parsed {0} lines from Excel.", nrOfRows);
                    }
                }

            }

        }

        private void CreateXML()
        {

            XmlNode rootNode = xmlDoc.CreateElement("ProcessPriceList");
            XmlNode node1 = null;
            XmlNode node2 = null;
            XmlNode node3 = null;
            XmlNode xmlPriceList = null;

            AddAttribute(xmlDoc, rootNode, "xmlns", "http://www.openapplications.org/oagis/10");
            AddAttribute(xmlDoc, rootNode, "releaseID", "10");
            AddAttribute(xmlDoc, rootNode, "versionID", "1");
            AddAttribute(xmlDoc, rootNode, "systemEnvironmentCode", "");

            xmlDoc.AppendChild(rootNode);

            // Application Area
            node1 = xmlDoc.CreateElement("ApplicationArea");
            rootNode.AppendChild(node1);

            node2 = xmlDoc.CreateElement("Sender");
            node3 = xmlDoc.CreateElement("ReferenceID");
            node3.InnerText = MarketId;
            node2.AppendChild(node3);
            node3 = null;
            node1.AppendChild(node2);

            node2 = xmlDoc.CreateElement("CreationDateTime");
            node2.InnerText = getCurrentTimeStamp();
            node1.AppendChild(node2);

            node2 = xmlDoc.CreateElement("BODID");
            node2.InnerText = GUID;
            node1.AppendChild(node2);

            // DataArea
            node1 = xmlDoc.CreateElement("DataArea");
            rootNode.AppendChild(node1);

            node2 = xmlDoc.CreateElement("Sync");
            node1.AppendChild(node2);

            xmlPriceList = xmlDoc.CreateElement("PriceList");
            node1.AppendChild(xmlPriceList);

            // Create header
            node1 = xmlDoc.CreateElement("PriceListHeader");
            xmlPriceList.AppendChild(node1);

            node2 = xmlDoc.CreateElement("ID");
            node2.InnerText = PriceListName;
            node1.AppendChild(node2);

            node2 = xmlDoc.CreateElement("CurrencyCode");
            node2.InnerText = Currency;
            node1.AppendChild(node2);

            // Loop Lines
            int nrOfRows = 0;

            foreach (var record in Lines)
            {
                XmlNode lineNode = xmlDoc.CreateElement("PriceListLine");

                node1 = xmlDoc.CreateElement("Item");
                AddChildNode(xmlDoc, node1, "ID", record.ItemNumber);
                xmlPriceList.AppendChild(node1);

                lineNode.AppendChild(node1);

                // -------- Unit Price -----
                node1 = xmlDoc.CreateElement("UnitPrice");

                // ID - unit
                node2 = xmlDoc.CreateElement("ID");
                node2.InnerText = record.Unit;
                AddAttribute(xmlDoc, node2, "typeCode", "unit");
                node1.AppendChild(node2);

                // IDSet - timeUnit
                node2 = xmlDoc.CreateElement("IDSet");
                node3 = xmlDoc.CreateElement("ID");
                if (record.TimeUnit != null && !record.TimeUnit.TrimEnd().Equals(""))
                {
                    node3.InnerText = record.TimeUnit;
                }
                AddAttribute(xmlDoc, node3, "typeCode", "timeunit");
                node2.AppendChild(node3);
                node1.AppendChild(node2);

                lineNode.AppendChild(node1);

                // Charge amount
                AddChildNode(xmlDoc, node1, "ChargeAmount", record.NewPrice);

                // TimePeriod
                node2 = xmlDoc.CreateElement("TimePeriod");
                AddChildNode(xmlDoc, node2, "StartDateTime", record.FormattedValidFromDate);
                node3 = xmlDoc.CreateElement("EndDateTime");
                node2.AppendChild(node3);
                node1.AppendChild(node2);

                // Extension
                string strIsFreeOfCharge = "N";
                if (record.NewPrice.Equals("0.00") || record.NewPrice.Equals("0.0") || record.NewPrice.Equals("0"))
                {
                    strIsFreeOfCharge = "Y";
                }

                node2 = xmlDoc.CreateElement("Extension");

                node3 = xmlDoc.CreateElement("Code");
                node3.InnerText = strIsFreeOfCharge;
                AddAttribute(xmlDoc, node3, "typeCode", "freeOfCharge");
                node2.AppendChild(node3);

                node3 = xmlDoc.CreateElement("ID");
                //node3.InnerText = "";
                AddAttribute(xmlDoc, node3, "typeCode", "VATcode");
                node2.AppendChild(node3);

                node3 = xmlDoc.CreateElement("Indicator");
                node3.InnerText = "true";
                AddAttribute(xmlDoc, node3, "typeCode", "VATbased");
                node2.AppendChild(node3);

                node1.AppendChild(node2);

                xmlPriceList.AppendChild(lineNode);

                nrOfRows++;
                int modValue = nrOfRows % modularTraceValue;

                if (modValue == 0)
                {
                    Trace.TraceInformation("Added {0} lines to XML result.", nrOfRows);
                }
                //Trace.TraceInformation("item: {0}, unit: {1}, price: {2}, timeunit: {3}, fromdate: {4}, formatted from date: {5}", record.ItemNumber, record.Unit, record.NewPrice, record.TimeUnit, record.ValidFromDate, record.FormattedValidFromDate);
            }

            //Trace.TraceInformation(xmlDoc.OuterXml.ToString());

        }

        private void AddAttribute(XmlDocument xmlDoc, XmlNode currentNode, string attribName, string attribValue)
        {
            XmlAttribute attr = xmlDoc.CreateAttribute(attribName);
            attr.Value = attribValue;

            currentNode.Attributes.SetNamedItem(attr);
        }

        private void AddChildNode(XmlDocument xmlDoc, XmlNode parentNode, string name, string value)
        {
            XmlNode tmpNode = xmlDoc.CreateElement(name);
            tmpNode.InnerText = value;
            parentNode.AppendChild(tmpNode);

        }

        private string getCurrentTimeStamp()
        {
            DateTime dtUtcNow = DateTime.UtcNow;

            return dtUtcNow.ToString("yyyy-MM-ddTHH:mm:ss+0000");
        }

        internal string getCurrentTimeStamp(string formatting)
        {
            DateTime dtUtcNow = DateTime.UtcNow;

            return dtUtcNow.ToString(formatting);
        }

        private string FormatDateToYMD8(string strFromDate, string fromDateFormat)
        {
            string returnValue = strFromDate;
            string destinationFormat = "yyyy-MM-dd";

            try
            {
                DateTime dt = DateTime.ParseExact(strFromDate, fromDateFormat, CultureInfo.InvariantCulture);
                returnValue = dt.ToString(destinationFormat);
            }
            catch (FormatException)
            {
                Trace.TraceWarning("Could not parse the date to given format. FromDate: {0}, Source format: {1}", strFromDate, fromDateFormat);
            }

            return returnValue;

        }

        private string GetLocalCellValue(WorkbookPart wbPart, WorksheetPart wsPart, string addressName)
        {
            string value = "";

            // Use its Worksheet property to get a reference to the cell 
            // whose address matches the address you supplied.
            Cell theCell = wsPart.Worksheet.Descendants<Cell>().
              Where(c => c.CellReference == addressName).FirstOrDefault();

            // If the cell does not exist, return an empty string.
            if (theCell != null)
            {
                value = theCell.InnerText;

                // If the cell represents an integer number, you are done. 
                // For dates, this code returns the serialized value that 
                // represents the date. The code handles strings and 
                // Booleans individually. For shared strings, the code 
                // looks up the corresponding value in the shared string 
                // table. For Booleans, the code converts the value into 
                // the words TRUE or FALSE.
                if (theCell.DataType != null)
                {
                    switch (theCell.DataType.Value)
                    {
                        case CellValues.SharedString:

                            // For shared strings, look up the value in the
                            // shared strings table.
                            var stringTable =
                                wbPart.GetPartsOfType<SharedStringTablePart>()
                                .FirstOrDefault();

                            // If the shared string table is missing, something 
                            // is wrong. Return the index that is in
                            // the cell. Otherwise, look up the correct text in 
                            // the table.
                            if (stringTable != null)
                            {
                                value =
                                    stringTable.SharedStringTable
                                    .ElementAt(int.Parse(value)).InnerText;
                            }
                            break;

                        case CellValues.Boolean:
                            switch (value)
                            {
                                case "0":
                                    value = "FALSE";
                                    break;
                                default:
                                    value = "TRUE";
                                    break;
                            }
                            break;
                    }
                }
            }

            return value;
        }


        public bool Send()
        {
            string xmlBody = xmlDoc.OuterXml.ToString();

            using (var client = new HttpClient())
            {

                string baseAddress = System.Configuration.ConfigurationManager.AppSettings["360.BaseAddress"];
                string uri = System.Configuration.ConfigurationManager.AppSettings["360.Uri.PriceList"];
                string subscriptionkey = System.Configuration.ConfigurationManager.AppSettings["360.SubscriptionKey"];

                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/xml"));

                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", subscriptionkey);

                Trace.TraceInformation($"Sending POST {uri}");
                //Trace.TraceInformation($"Body: " + xmlBody);

                HttpResponseMessage response = client.PostAsync(uri, new StringContent(xmlBody)).Result;

                parameters.Add("Message Body to 360", xmlBody);

                if (!response.IsSuccessStatusCode)
                {
                    Trace.TraceError($"Rental says: {response.StatusCode}");
                    Trace.TraceError($"Rental says: {response.Content.ReadAsStringAsync().Result}");

                    parameters.Add("HTTP Status code", response.StatusCode.ToString());
                    parameters.Add("HTTP Response", response.Content.ReadAsStringAsync().Result);

                    telemetry.TrackEvent("Price list not sent ok", parameters);
                    return false;
                }
                else
                {
                    Trace.TraceInformation($"Rental says: {response.StatusCode}");
                    Trace.TraceInformation($"Rental says: {response.Content.ReadAsStringAsync().Result}");

                    parameters.Add("HTTP Status code", response.StatusCode.ToString());

                    telemetry.TrackEvent("Price list updated OK", parameters);
                    return true;
                }

            }

            
        }

        class ListRecord
        {
            public String ItemNumber { get; set; }
            public String Unit { get; set; }
            public String NewPrice { get; set; }
            public String TimeUnit { get; set; }
            public String ValidFromDate { get; set; }
            public String FormattedValidFromDate { get; set; }
        }
    }
}
