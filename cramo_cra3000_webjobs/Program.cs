﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Azure.WebJobs.ServiceBus;
using Microsoft.ServiceBus;
using Microsoft.Azure.WebJobs.Extensions.SendGrid;


namespace cramo_cra3000_webjobs
{
    // To learn more about Microsoft Azure WebJobs SDK, please see http://go.microsoft.com/fwlink/?LinkID=320976
    class Program
    {

        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main()
        {

            string serviceBusConnectionString = AmbientConnectionStringProvider.Instance.GetConnectionString(ConnectionStringNames.ServiceBus);

            JobHostConfiguration config = new JobHostConfiguration();

            ServiceBusConfiguration sb_config = new ServiceBusConfiguration();

            sb_config.ConnectionString = serviceBusConnectionString;

            sb_config.MessageOptions = new Microsoft.ServiceBus.Messaging.OnMessageOptions
            {
                AutoRenewTimeout = TimeSpan.FromMinutes(10),
                AutoComplete = false,
                MaxConcurrentCalls = 10

            };


            config.UseServiceBus(sb_config);

            config.UseCore();

            JobHost host = new JobHost(config);

            host.RunAndBlock();
        }
    }
}
